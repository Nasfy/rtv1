.PHONY: all clean fclean re pclean pre

CC=clang
CFLAGS=-Wall -Wextra -Werror -Ilibft -Iincludes
NAME=RTv1
OBJDIR=objs
SRCDIR=srcs

SRC=main.c intersection.c rgb.c funcs.c square_equation.c calc_color.c \
	 figure_intersections.c figure_normals.c \
	 ray.c ray_funcs.c ray_math.c rotation.c cam_move.c\
	 readfile_array_funcs.c readfile_light_ambient.c \
	 readfile_light_parser_help.c readfile_parsing.c \
	 readfile_sphere.c readfile_cone.c readfile_light_direct.c \
	 readfile_objects_parser.c readfile_plane.c readfile_str_funcs.c \
	 readfile_cylinder.c readfile_light_parser.c \
	 readfile_objects_parser_help.c readfile_scene_parser.c

OBJ=$(SRC:.c=.o)
LIBFT=libft/libft.a

INCLUDE=-framework OpenGL -framework AppKit -I/usr/local/include -L/usr/local/lib -lmlx

all: $(OBJDIR) $(NAME)

$(OBJDIR):
	@mkdir -p objs/

$(NAME): $(addprefix $(OBJDIR)/, $(OBJ)) $(LIBFT)
	@$(CC) $(CFLAGS) $(INCLUDE) -o $@ $^ 

$(addprefix $(OBJDIR)/, %.o): $(addprefix $(SRCDIR)/, %.c)
	@$(CC) $(CFLAGS) -c -o $@ $< -g

debug:
	$(CC) $(CFLAGS) $(INCLUDE) $(addprefix $(SRCDIR)/, $(SRC)) -g libft/libft.a -o $(NAME)

$(LIBFT):
	@$(MAKE) --no-print-directory -C libft

clean:
	@$(MAKE) --no-print-directory -C libft clean
	@rm -rf $(OBJDIR)
	@echo "cleaned"

fclean:
	@$(MAKE) --no-print-directory -C libft fclean
	@rm -rf $(OBJDIR)
	@rm -f $(NAME)
	@echo "fcleaned"

re: fclean all
