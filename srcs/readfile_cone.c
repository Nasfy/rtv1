/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   readfile_cone.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiriuk <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 15:58:06 by abiriuk           #+#    #+#             */
/*   Updated: 2018/11/25 18:08:33 by abiriuk          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"
#include <math.h>

static int	cone_lines2(char **arr, int *save, t_figure *obj)
{
	int		ret;

	ret = 1;
	if (ft_strequ(arr[0], "axis"))
	{
		obj->axis = ray_normalize(new_vector(ft_atof(arr[1]), ft_atof(arr[2]),
					ft_atof(arr[3])));
		*save |= 0b100;
	}
	else
		ret = help(obj, arr);
	return (ret);
}

static int	cone_lines(t_all *all, int *save, t_figure *obj)
{
	char	**const	arr = get_line_arr(all->fd, 4);
	int				ret;

	ret = 1;
	if (ft_strequ(arr[0], "origin"))
	{
		obj->center = new_vector(ft_atof(arr[1]), ft_atof(arr[2]),
					ft_atof(arr[3]));
		*save |= 0b1;
	}
	else if (ft_strequ(arr[0], "angle"))
	{
		obj->tg = ft_atoi(arr[1]);
		if (obj->tg > 178 || obj->tg < 1)
			errmsg("Cone angle must be 1-178");
		obj->tg = pow(tan(get_radian(obj->tg / 2)), 2);
		*save |= 0b10;
	}
	else
		ret = cone_lines2(arr, save, obj);
	free2arr(arr);
	return (ret);
}

void		read_cone(t_all *all, t_figure *obj)
{
	int			save;

	save = 0;
	while (cone_lines(all, &save, obj))
		;
	if (save != 0b111)
		errmsg("Need more information to create cone");
	obj->type = CONE;
}
