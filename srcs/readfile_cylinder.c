/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   readfile_cylinder.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiriuk <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 15:58:25 by abiriuk           #+#    #+#             */
/*   Updated: 2018/11/25 18:08:46 by abiriuk          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"
#include <math.h>

static int	obj_lines2(char **arr, int *save, t_figure *obj)
{
	int		ret;

	ret = 1;
	if (ft_strequ(arr[0], "axis"))
	{
		obj->axis = ray_normalize(new_vector(ft_atof(arr[1]),
					ft_atof(arr[2]), ft_atof(arr[3])));
		*save |= 0b100;
	}
	else
		ret = help(obj, arr);
	return (ret);
}

static int	cylinder_lines(t_all *all, int *save, t_figure *obj)
{
	char	**const	arr = get_line_arr(all->fd, 4);
	int				ret;

	ret = 1;
	if (ft_strequ(arr[0], "origin"))
	{
		obj->center = new_vector(ft_atof(arr[1]), ft_atof(arr[2]),
					ft_atof(arr[3]));
		*save |= 0b1;
	}
	else if (ft_strequ(arr[0], "radius"))
	{
		obj->pow_radius = pow(ft_atof(arr[1]), 2);
		*save |= 0b10;
	}
	else
		ret = obj_lines2(arr, save, obj);
	free2arr(arr);
	return (ret);
}

void		read_cylinder(t_all *all, t_figure *obj)
{
	int			save;

	save = 0;
	while (cylinder_lines(all, &save, obj))
		;
	if (save != 0b111)
		errmsg("Need more information to create cylinder");
	obj->type = CYLINDER;
}
