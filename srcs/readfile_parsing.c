/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   readfile_parsing.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiriuk <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 16:01:58 by abiriuk           #+#    #+#             */
/*   Updated: 2018/11/25 18:10:37 by abiriuk          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"
#include <fcntl.h>

static void		choose_parser(t_all *all)
{
	char	*str;

	str = get_line(all->fd);
	if (ft_strequ(str, "scene:"))
		scene_parser(all);
	else
		errmsg("Scene parser must be first");
	free(str);
	str = get_line(all->fd);
	if (ft_strequ(str, "objects:"))
		objects_parser(all);
	else
		errmsg("Objects parser must be second");
	free(str);
	str = get_line(all->fd);
	if (ft_strequ(str, "light:"))
		light_parser(all);
	else
		errmsg("Light parser must be last");
	free(str);
}

static int		open_file(char *str)
{
	char	**arr;
	int		len;
	int		fd;

	if (!(arr = ft_strsplit(str, '.')))
		errmsg("Cannot allocate memory");
	len = len2arr(arr);
	if (len < 2 || !ft_strequ(arr[len - 1], "rt"))
		errmsg("Wrong allname. It must be like \"Name.rt\"");
	free2arr(arr);
	if ((fd = open(str, O_RDONLY)) < 3)
		errmsg("Cannot open all");
	return (fd);
}

static void		initialization(t_all *all, char *str)
{
	all->objs = NULL;
	all->lights = NULL;
	all->pic = NULL;
	all->mlx = NULL;
	all->win = NULL;
	all->objlist = NULL;
	all->lightlist = NULL;
	all->face = (t_camera *)malloc(sizeof(t_camera));
	if (all->face == NULL)
		errmsg("Cannot allocate memory");
	all->fd = open_file(str);
}

void			last_prep(t_all *all)
{
	all->face->map_x = all->scene_size.x;
	all->face->map_y = all->scene_size.y;
	all->num_pixels = all->face->map_x * all->face->map_y;
	all->face->screen_distance = 1;
	all->face->screen_width = 1;
	all->face->screen_height = (double)all->face->map_x / all->face->map_y;
	all->face->rel_x = all->face->screen_height / (double)all->face->map_x;
	all->face->rel_y = all->face->screen_width / (double)all->face->map_y;
}

void			parsing(t_all *all, char *str)
{
	char	*tmp;

	initialization(all, str);
	choose_parser(all);
	if (get_next_line(all->fd, &tmp))
		errmsg("Wrong all");
	close(all->fd);
	last_prep(all);
}
