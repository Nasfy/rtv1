/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ray_math.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiriuk <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 15:57:33 by abiriuk           #+#    #+#             */
/*   Updated: 2018/11/25 18:08:06 by abiriuk          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

cl_double4	vector_add(cl_double4 vector, cl_double4 num)
{
	return (new_vector(vector.x + num.x, vector.y + num.y, vector.z + num.z));
}

cl_double4	vector_sub(cl_double4 vector, cl_double4 num)
{
	return (new_vector(vector.x - num.x, vector.y - num.y, vector.z - num.z));
}

cl_double4	ray_mult(cl_double4 ray, double num)
{
	return (new_vector(ray.x * num, ray.y * num, ray.z * num));
}
