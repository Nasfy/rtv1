/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   readfile_light_parser_help.c                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiriuk <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 16:00:01 by abiriuk           #+#    #+#             */
/*   Updated: 2018/11/25 18:09:55 by abiriuk          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

int				light_help(t_light *light, char **const arr)
{
	if (ft_strequ(arr[0], "]"))
		return (0);
	if (ft_strequ(arr[0], "brightness"))
	{
		light->intensity = ft_atof(arr[1]);
		if (light->intensity < 0 || light->intensity > 1)
			errmsg("Light intensity can have value from 0 to 1");
	}
	return (1);
}
