/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cam_move.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiriuk <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 18:31:28 by abiriuk           #+#    #+#             */
/*   Updated: 2018/11/25 18:44:53 by abiriuk          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

static const int	g_speed = 1;

int		cam_move(int key, void *p)
{
	t_all *const	all = get_all(NULL);
	cl_double4		pos;
	double			move;

	if (p)
		;
	pos = new_vector(0, 0, 0);
	if (key == KEY_A || key == KEY_S || key == KEY_LSHIFT)
		move = -g_speed;
	else if (key == KEY_D || key == KEY_W || key == KEY_SPACE)
		move = g_speed;
	if (key == KEY_A || key == KEY_D)
		pos.x += move;
	else if (key == KEY_SPACE || key == KEY_LSHIFT)
		pos.y += move;
	else if (key == KEY_W || key == KEY_S)
		pos.z += move;
	else
		return (0);
	pos = rotate_vector(pos, all->face->rotation);
	all->face->point.x += pos.x;
	all->face->point.y += pos.y;
	all->face->point.z += pos.z;
	zloop(all);
	return (1);
}
