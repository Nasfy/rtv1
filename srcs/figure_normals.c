/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   figure_normals.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiriuk <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 15:46:41 by abiriuk           #+#    #+#             */
/*   Updated: 2018/11/25 18:05:39 by abiriuk          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

cl_double4		cone_normal(t_figure *obj, t_ray *ray, double d)
{
	cl_double4		normal;
	double			h1;
	double			h2;
	cl_double4		h3;

	h1 = ray_product(ray->ray, obj->axis);
	h2 = ray_product(vector_sub(ray->start, obj->center), obj->axis);
	h3 = ray_mult(obj->axis, (obj->tg + 1) * (h1 * d + h2));
	normal = vector_sub(ray->dest, vector_add(obj->center, h3));
	return (ray_normalize(normal));
}

cl_double4		cyl_normal(t_figure *obj, t_ray *ray, double d)
{
	cl_double4			normal;
	double				h1;
	double				h2;
	cl_double4			h3;

	h1 = ray_product(ray->ray, obj->axis);
	h2 = ray_product(vector_sub(ray->start, obj->center), obj->axis);
	h3 = ray_mult(obj->axis, h1 * d + h2);
	normal = vector_sub(ray->dest, vector_add(obj->center, h3));
	return (ray_normalize(normal));
}

cl_double4		plane_normal(t_figure *obj, t_ray *ray)
{
	if (ray_product(ray->ray, obj->normal) > 0)
		return (ray_mult(obj->normal, -1));
	return (obj->normal);
}

cl_double4		sphere_normal(t_figure *obj, t_ray *ray)
{
	return (ray_normalize(vector_sub(ray->dest, obj->center)));
}
