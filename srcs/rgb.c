/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rgb.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiriuk <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 15:53:23 by abiriuk           #+#    #+#             */
/*   Updated: 2018/11/25 18:12:02 by abiriuk          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

int			rgb_to_int(cl_int4 c)
{
	return ((c.x << 16) | (c.y << 8) | c.z);
}

cl_int4		new_color(int r, int g, int b)
{
	cl_int4	rgb;

	rgb.x = r;
	rgb.y = g;
	rgb.z = b;
	return (rgb);
}
