/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   figure_intersections.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiriuk <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 15:46:13 by abiriuk           #+#    #+#             */
/*   Updated: 2018/11/25 18:46:30 by abiriuk          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"
#include <math.h>

int		cone_intersection(t_figure *obj, t_ray *ray, double *t)
{
	cl_double4			k;
	const double		r_axis = ray_product(ray->ray, obj->axis);
	const double		cam_cone_axis =

	ray_product(vector_sub(ray->start, obj->center), obj->axis);
	k.x = ray->len - (1 + obj->tg) * pow(r_axis, 2);
	k.y = 2 * (ray_product(ray->ray, vector_sub(ray->start, obj->center)) -
			(obj->tg + 1) * r_axis * cam_cone_axis);
	k.z = ray_pow(vector_sub(ray->start, obj->center)) - (obj->tg + 1) *
		pow(cam_cone_axis, 2);
	if (!sq_equation(k, t))
		return (0);
	return (1);
}

int		cylinder_intersection(t_figure *obj, t_ray *ray, double *t)
{
	cl_double4				k;
	const double			r_axis = ray_product(ray->ray, obj->axis);
	const double			cam_cyl_axis =

	ray_product(vector_sub(ray->start, obj->center), obj->axis);
	k.x = ray->len - pow(r_axis, 2);
	k.y = 2 * (ray_product(ray->ray, vector_sub(ray->start, obj->center)) -
			(r_axis * cam_cyl_axis));
	k.z = ray_pow(vector_sub(ray->start, obj->center)) - obj->pow_radius -
			pow(cam_cyl_axis, 2);
	if (!sq_equation(k, t))
		return (0);
	return (1);
}

int		plane_intersection(t_figure *obj, t_ray *ray, double *t)
{
	double			d_v;
	double			x_v;

	d_v = ray_product(ray->ray, obj->normal);
	if (d_v == 0)
		return (0);
	x_v = ray_product(vector_sub(obj->center, ray->start), obj->normal);
	if ((d_v < 0 ? -1 : 1) != (x_v < 0 ? -1 : 1))
		return (0);
	*t = x_v / d_v;
	return (1);
}

int		sphere_intersection(t_figure *obj, t_ray *ray, double *t)
{
	const cl_double4		cam_sphere = vector_sub(ray->start, obj->center);
	cl_double4				k;

	k.x = ray->len;
	k.y = 2 * ray_product(ray->ray, cam_sphere);
	k.z = ray_pow(vector_sub(ray->start, obj->center)) - obj->pow_radius;
	if (!sq_equation(k, t))
		return (0);
	return (1);
}
