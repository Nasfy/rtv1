/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ray_funcs.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiriuk <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 15:57:18 by abiriuk           #+#    #+#             */
/*   Updated: 2018/11/25 18:07:51 by abiriuk          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"
#include <math.h>

double		ray_product(cl_double4 vect1, cl_double4 vect2)
{
	return (vect1.x * vect2.x + vect1.y * vect2.y + vect1.z * vect2.z);
}

double		ray_pow(cl_double4 vect)
{
	return ((vect.x * vect.x) + (vect.y * vect.y) + (vect.z * vect.z));
}

double		ray_length(cl_double4 vect)
{
	double	ret;

	ret = (vect.x * vect.x) + (vect.y * vect.y) + (vect.z * vect.z);
	return (sqrt(ret));
}

double		ray_cos(cl_double4 vect1, cl_double4 vect2)
{
	double	save;

	if ((save = ray_product(vect1, vect2)) <= 0)
		return (-1);
	save /= ray_length(vect1);
	save /= ray_length(vect2);
	return (save);
}

cl_double4	ray_normalize(cl_double4 vect)
{
	const double	len = ray_length(vect);

	if (len < 0.00001 && len > -0.00001)
		return (new_vector(0, 0, 0));
	vect.x /= len;
	vect.y /= len;
	vect.z /= len;
	return (vect);
}
