/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/03 17:20:30 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/25 18:35:58 by abiriuk          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"
#include <time.h>

static int	exit1(void *elem)
{
	t_all	*all;

	all = (t_all *)elem;
	mlx_destroy_window(all->mlx, all->win);
	exit(1);
	return (0);
}

static int	exit2(int key, void *elem)
{
	t_all	*all;

	if (key == KEY_ESCAPE)
	{
		all = (t_all *)elem;
		mlx_destroy_window(all->mlx, all->win);
		exit(1);
	}
	return (0);
}

static int	image(int map_x, int map_y, t_picture *pic, void *mlx)
{
	int		endian;
	int		line_size;

	pic->size = map_x * map_y;
	pic->pix_size = 8;
	line_size = pic->pix_size * map_x;
	endian = 0;
	pic->ptr = mlx_new_image(mlx, map_x, map_y);
	if (pic->ptr == NULL)
		return (1);
	pic->arr = (int *)mlx_get_data_addr(pic->ptr, &(pic->pix_size),
			&(line_size), &(endian));
	if (pic->arr == NULL)
		return (1);
	return (0);
}

int			mlx(t_all *all)
{
	all->mlx = mlx_init();
	if (all->mlx == NULL)
		return (1);
	all->win = mlx_new_window(all->mlx, all->face->map_x,
			all->face->map_y, all->name);
	if (all->win == NULL)
		return (1);
	mlx_hook(all->win, 17, 1L << 17, exit1, (void *)all);
	mlx_key_hook(all->win, exit2, (void *)all);
	if ((all->pic = (t_picture *)malloc(sizeof(t_picture))) == NULL)
		return (1);
	if (image(all->face->map_x, all->face->map_y, all->pic, all->mlx))
		return (1);
	return (0);
}

int			main(int ac, char **av)
{
	t_all	all;

	if (ac != 2)
	{
		write(2, USAGE, ft_strlen(USAGE));
		return (EXIT_FAILURE);
	}
	get_all(&all);
	parsing(&all, av[1]);
	convert_to_array(all.objlist, all.lightlist, &all);
	mlx(&all);
	mlx_hook(all.win, 2, 5, cam_move, NULL);
	zloop(&all);
	mlx_loop(all.mlx);
}
