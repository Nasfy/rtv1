/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   readfile_str_funcs.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiriuk <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 16:02:51 by abiriuk           #+#    #+#             */
/*   Updated: 2018/11/25 18:11:39 by abiriuk          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

char	*trim_str(char *str)
{
	char	*trim;

	if (str == NULL)
		return (NULL);
	if (!(trim = ft_strtrim(str)))
		errmsg("Cannot allocate memory");
	free(str);
	return (trim);
}

char	*get_line(int fd)
{
	char	*str;

	str = NULL;
	if ((get_next_line(fd, &str)) < 1)
		errmsg("Wrong file");
	if (!(str = trim_str(str)))
		errmsg("Wrong file");
	while (str && (str[0] == 0 || str[0] == '#'))
	{
		free(str);
		str = NULL;
		if ((get_next_line(fd, &str)) < 1)
			errmsg("Wrong file");
		if (!(str = trim_str(str)))
			errmsg("Wrong file");
	}
	return (str);
}

char	**get_line_arr(int fd, int len)
{
	char	**arr;
	char	*str;

	str = get_line(fd);
	arr = ft_strsplit(str, ' ');
	if (arr == NULL)
		errmsg("Cannot allocate memory");
	if (len < len2arr(arr))
		errmsg("Wrong file");
	free(str);
	return (arr);
}
