/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   readfile_sphere.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiriuk <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 16:02:38 by abiriuk           #+#    #+#             */
/*   Updated: 2018/11/25 18:11:24 by abiriuk          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"
#include <math.h>

static int	sphere_lines(t_all *all, int *save, t_figure *obj)
{
	char	**const	arr = get_line_arr(all->fd, 4);
	int				ret;

	ret = 1;
	if (ft_strequ(arr[0], "origin"))
	{
		obj->center = new_vector(ft_atof(arr[1]), ft_atof(arr[2]),
					ft_atof(arr[3]));
		*save |= 0b1;
	}
	else if (ft_strequ(arr[0], "radius"))
	{
		obj->pow_radius = pow(ft_atof(arr[1]), 2);
		*save |= 0b10;
	}
	else
		ret = help(obj, arr);
	free2arr(arr);
	return (ret);
}

void		read_sphere(t_all *all, t_figure *obj)
{
	int			save;

	save = 0;
	while (sphere_lines(all, &save, obj))
		;
	if (save != 0b11)
		errmsg("You have to type more information to create sphere");
	obj->type = SPHERE;
}
