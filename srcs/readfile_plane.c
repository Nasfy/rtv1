/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   readfile_plane.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiriuk <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 16:02:11 by abiriuk           #+#    #+#             */
/*   Updated: 2018/11/25 18:10:56 by abiriuk          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

static int	plane_lines(t_all *all, int *save, t_figure *obj)
{
	int		ret;
	char	**arr;

	ret = 1;
	arr = get_line_arr(all->fd, 4);
	if (ft_strequ(arr[0], "origin"))
	{
		obj->center = new_vector(ft_atof(arr[1]), ft_atof(arr[2]),
				ft_atof(arr[3]));
		*save |= 0b1;
	}
	else if (ft_strequ(arr[0], "normal"))
	{
		obj->normal = ray_normalize(new_vector(ft_atof(arr[1]),
					ft_atof(arr[2]), ft_atof(arr[3])));
		*save |= 0b10;
	}
	else
		ret = help(obj, arr);
	free2arr(arr);
	return (ret);
}

void		read_plane(t_all *all, t_figure *obj)
{
	int			save;

	save = 0;
	while (plane_lines(all, &save, obj))
		;
	if (save != 0b11)
		errmsg("Need more information to allocate plane");
	obj->type = PLANE;
}
