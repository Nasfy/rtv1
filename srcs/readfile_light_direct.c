/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   readfile_light_direct.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiriuk <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 15:59:10 by abiriuk           #+#    #+#             */
/*   Updated: 2018/11/25 18:09:09 by abiriuk          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

static int		direct_lines(t_all *all, t_light *light, int *save)
{
	char **const	arr = get_line_arr(all->fd, 4);
	int				ret;

	ret = 1;
	if (ft_strequ(arr[0], "position"))
	{
		light->position = new_vector(ft_atof(arr[1]), ft_atof(arr[2]),
				ft_atof(arr[3]));
		*save = 1;
	}
	else
		ret = light_help(light, arr);
	free2arr(arr);
	return (ret);
}

void			parse_direct(t_all *all, t_light *light)
{
	int		save;

	save = 0;
	while (direct_lines(all, light, &save))
		;
	if (save != 1)
		errmsg("Light position missing");
	light->type = 1;
}
