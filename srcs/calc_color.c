/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   calc_color.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiriuk <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 15:46:06 by abiriuk           #+#    #+#             */
/*   Updated: 2018/11/25 18:46:12 by abiriuk          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"
#include <math.h>

double	obj_col(cl_double4 dest, cl_double4 normal, t_light *ls)
{
	cl_double4	light_ray;
	double		intensity;

	light_ray = vector_sub(ls->position, dest);
	intensity = ray_cos(normal, light_ray);
	if (intensity <= 0)
		return (0);
	intensity = ls->intensity * intensity;
	return (intensity);
}

double	light_col(cl_double4 normal, t_ray *ray, t_light *ls, t_figure *save)
{
	cl_double4	l_path;
	cl_double4	mid_vector;
	double		res;

	if (save->specularity < 0)
		return (0);
	l_path = vector_sub(ls->position, ray->dest);
	mid_vector = vector_sub(
			ray_mult(normal, 2 * ray_product(normal, l_path)), l_path);
	res = ray_cos(ray_mult(ray->ray, -1), mid_vector);
	if (res <= 0)
		return (0);
	res = ls->intensity * pow(res, save->specularity);
	return (res);
}

double	get_shadow(t_ray ray, t_figure *objs, t_figure *save)
{
	double			distance;

	while (objs->type)
	{
		if (save != objs && choose_intersect(objs, &ray, &distance))
			if (distance <= 1 && distance > 0)
				return (0);
		objs++;
	}
	return (1);
}

cl_int4	mult_color(cl_int4 color, double diffuse, double spec)
{
	color.x = (int)(floor(color.x * diffuse) + floor(255 * spec));
	color.y = (int)(floor(color.y * diffuse) + floor(255 * spec));
	color.z = (int)(floor(color.z * diffuse) + floor(255 * spec));
	color.x = color.x > 255 ? 255 : color.x;
	color.x = color.x < 0 ? 0 : color.x;
	color.y = color.y > 255 ? 255 : color.y;
	color.y = color.y < 0 ? 0 : color.y;
	color.z = color.z > 255 ? 255 : color.z;
	color.z = color.z < 0 ? 0 : color.z;
	return (color);
}

cl_int4	calc_color(t_ray *ray, double distance, t_figure *save)
{
	t_all *const	all = get_all(NULL);
	t_light			*ls;
	double			i_s[2];
	cl_double4		normal;

	ls = all->lights;
	i_s[0] = all->ambient_light;
	i_s[1] = 0;
	normal = choose_normal(save, ray, distance);
	while (ls->type)
	{
		if (get_shadow(new_line2(ray->dest, ls->position), all->objs, save))
		{
			i_s[0] += obj_col(ray->dest, normal, ls);
			i_s[1] += light_col(normal, ray, ls, save);
		}
		ls++;
	}
	return (mult_color(save->color, i_s[0], i_s[1]));
}
