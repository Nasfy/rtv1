/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   readfile_objects_parser.c                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiriuk <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 16:01:07 by abiriuk           #+#    #+#             */
/*   Updated: 2018/11/25 18:10:10 by abiriuk          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

const static int	g_len = 4;

static void			(*g_funcs[g_len])(t_all *, t_figure *) =
{read_plane, read_sphere, read_cylinder, read_cone};

static char			*(g_names[g_len]) =
{"plane:", "sphere:", "cylinder:", "cone:"};

static t_figure	init_figure(int fd)
{
	t_figure	obj;
	char *const str = get_line(fd);

	if (!ft_strequ(str, "["))
		errmsg("Wrong object representation");
	obj.color = new_color(255, 255, 255);
	obj.specularity = -1;
	free(str);
	return (obj);
}

static int		choose_way(t_all *all, char *str)
{
	t_figure	obj;
	t_list		*lst;
	int			i;

	i = -1;
	while (++i < g_len)
		if (ft_strequ(str, g_names[i]))
		{
			obj = init_figure(all->fd);
			g_funcs[i](all, &obj);
			if (!(lst = ft_lstnew(&obj, sizeof(t_figure))))
				errmsg("Cannot allocate memory");
			ft_lstadd(&(all->objlist), lst);
			return (0);
		}
	if (ft_strequ(str, "{"))
		return (0);
	else if (ft_strequ(str, "}"))
		return (1);
	else
		errmsg("Wrong information in objects parser");
	return (0);
}

void			objects_parser(t_all *all)
{
	char		*str;

	while (1)
	{
		str = get_line(all->fd);
		if (choose_way(all, str))
		{
			free(str);
			break ;
		}
		free(str);
	}
}
