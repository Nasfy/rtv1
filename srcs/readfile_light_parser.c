/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   readfile_light_parser.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiriuk <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 15:59:41 by abiriuk           #+#    #+#             */
/*   Updated: 2018/11/25 18:09:29 by abiriuk          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

const static int	g_len = 2;

static void			(*g_funcs[g_len])(t_all *, t_light *) =
{parse_direct, parse_ambient};

static char			*(g_names[g_len]) = {"direct:", "ambient:"};

static t_light		init_light(int fd)
{
	t_light			light;
	char *const		str = get_line(fd);

	if (!ft_strequ(str, "["))
		errmsg("Wrong light representation");
	light.intensity = 0;
	free(str);
	return (light);
}

static int			choose_way(t_all *all, char *str)
{
	int			i;
	t_light		light;
	t_list		*lst;

	i = -1;
	while (++i < g_len)
		if (ft_strequ(str, g_names[i]))
		{
			light = init_light(all->fd);
			g_funcs[i](all, &light);
			if (i == 0)
			{
				if (!(lst = ft_lstnew(&light, sizeof(t_figure))))
					errmsg("Cannot allocate memory");
				ft_lstadd(&(all->lightlist), lst);
			}
			return (0);
		}
	if (ft_strequ(str, "{"))
		return (0);
	else if (ft_strequ(str, "}"))
		return (1);
	else
		errmsg("Wrong infomation in light parser");
	return (0);
}

void				light_parser(t_all *all)
{
	char		*str;

	all->ambient_light = 0;
	while (1)
	{
		str = get_line(all->fd);
		if (choose_way(all, str))
		{
			free(str);
			break ;
		}
		free(str);
	}
}
