/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   square_equation.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiriuk <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 16:03:35 by abiriuk           #+#    #+#             */
/*   Updated: 2018/11/25 18:12:33 by abiriuk          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"
#include <math.h>

double	get_radian(double angle)
{
	return (M_PI * angle / 180);
}

int		sq_equation(cl_double4 k, double *res)
{
	double	t[2];
	double	discriminant;
	double	tmp;

	discriminant = pow(k.y, 2) - 4 * k.x * k.z;
	if (discriminant < 0)
		return (0);
	else if (discriminant > 0)
		discriminant = sqrt(discriminant);
	t[0] = (-k.y - discriminant) / k.x / 2;
	t[1] = (-k.y + discriminant) / k.x / 2;
	if (t[0] < 0 && t[1] < 0)
		return (0);
	*res = t[0] < t[1] ? t[0] : t[1];
	tmp = t[0] > t[1] ? t[0] : t[1];
	if (*res < tmp && *res < 0)
		*res = tmp;
	return (1);
}
