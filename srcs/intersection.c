/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_loop.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/19 18:30:12 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/25 18:22:17 by abiriuk          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"
#include <math.h>

int			choose_intersect(t_figure *obj, t_ray *ray, double *t)
{
	if (obj->type == SPHERE)
		return (sphere_intersection(obj, ray, t));
	else if (obj->type == PLANE)
		return (plane_intersection(obj, ray, t));
	else if (obj->type == CYLINDER)
		return (cylinder_intersection(obj, ray, t));
	else if (obj->type == CONE)
		return (cone_intersection(obj, ray, t));
	return (0);
}

cl_double4	choose_normal(t_figure *save, t_ray *ray, double distance)
{
	if (save->type == SPHERE)
		return (sphere_normal(save, ray));
	else if (save->type == PLANE)
		return (plane_normal(save, ray));
	else if (save->type == CYLINDER)
		return (cyl_normal(save, ray, distance));
	else if (save->type == CONE)
		return (cone_normal(save, ray, distance));
	return (new_vector(0, 0, 0));
}

int			intersect_point(t_ray *ray, t_figure *obj)
{
	double		len;
	double		dist;
	t_figure	*save;

	save = NULL;
	len = 0;
	dist = INFINITY;
	while (obj->type)
	{
		if (choose_intersect(obj, ray, &len) && len <= dist && len >= 0)
		{
			dist = len;
			save = obj;
		}
		obj++;
	}
	if (save == NULL)
		return (0);
	ray->dest = vector_add(ray->start, (ray_mult(ray->ray, dist)));
	return (rgb_to_int(calc_color(ray, dist, save)));
}

void		zloop(t_all *all)
{
	int				i;
	t_ray			ray;
	t_camera *const	face = all->face;
	cl_double4		vec;

	i = -1;
	while (++i < all->num_pixels)
	{
		vec = view3d(face, i / face->map_x, i % face->map_x);
		ray = new_line(face->point, vec);
		all->pic->arr[i] = intersect_point(&ray, all->objs);
	}
	mlx_put_image_to_window(all->mlx, all->win, all->pic->ptr, 0, 0);
}
