/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ray.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiriuk <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 15:56:57 by abiriuk           #+#    #+#             */
/*   Updated: 2018/11/25 18:07:31 by abiriuk          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

cl_double4		new_vector(double x, double y, double z)
{
	cl_double4	vector;

	vector.x = x;
	vector.y = y;
	vector.z = z;
	return (vector);
}

t_ray			new_line(cl_double4 start, cl_double4 direction)
{
	t_ray	line;

	line.ray = ray_normalize(vector_sub(direction, start));
	line.len = ray_pow(line.ray);
	line.start = start;
	return (line);
}

t_ray			new_line2(cl_double4 start, cl_double4 direction)
{
	t_ray	line;

	line.start = start;
	line.ray = vector_sub(direction, start);
	line.len = ray_pow(line.ray);
	return (line);
}
