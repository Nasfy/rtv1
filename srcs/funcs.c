/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   funcs.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiriuk <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 15:51:00 by abiriuk           #+#    #+#             */
/*   Updated: 2018/11/25 18:05:48 by abiriuk          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"
#include <stdio.h>

t_all		*get_all(t_all *all)
{
	static t_all	*allo;

	if (all)
		allo = all;
	return (allo);
}

void		errmsg(const char *str)
{
	perror(str);
	exit(EXIT_FAILURE);
}

cl_double4	view3d(t_camera *cam, int y, int x)
{
	cl_double4		vector;

	vector.x = (-cam->map_x / 2 + x) * cam->rel_x;
	vector.y = (cam->map_y / 2 - y) * cam->rel_y;
	vector.z = cam->screen_distance;
	vector = rotate_vector(vector, cam->rotation);
	vector = vector_add(vector, cam->point);
	return (vector);
}

static void	convert_lights(t_list *light, t_all *all)
{
	int		i;

	i = 0;
	all->num_lights = ft_lstlen(light);
	if (!(all->lights = (t_light *)ft_memalloc(sizeof(t_light) *
					(all->num_lights + 1))))
		errmsg("Cannot allocate enough memory");
	while (light)
	{
		all->lights[i] = *((t_light *)light->content);
		light = light->next;
		i++;
	}
	all->lights[all->num_lights].type = 0;
	ft_lstdel(&light, ft_lstdelcontent);
}

void		convert_to_array(t_list *obj, t_list *light, t_all *all)
{
	size_t			i;

	i = 0;
	all->num_objs = ft_lstlen(obj);
	if (!(all->objs = (t_figure *)ft_memalloc(sizeof(t_figure) *
					(all->num_objs + 1))))
		errmsg("Cannot allocate enough memory");
	while (obj)
	{
		all->objs[i] = *((t_figure *)obj->content);
		obj = obj->next;
		i++;
	}
	all->objs[all->num_objs].type = 0;
	ft_lstdel(&obj, ft_lstdelcontent);
	convert_lights(light, all);
}
