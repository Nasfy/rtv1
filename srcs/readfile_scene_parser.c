/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   readfile_scene_parser.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiriuk <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 16:02:24 by abiriuk           #+#    #+#             */
/*   Updated: 2018/11/25 18:11:09 by abiriuk          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

static void		parse_camera(t_all *all)
{
	char	**arr;
	int		i;

	i = 0;
	while (i < 2)
	{
		arr = get_line_arr(all->fd, 4);
		if (ft_strequ(arr[0], "origin"))
		{
			all->face->point.x = ft_atoi(arr[1]);
			all->face->point.y = ft_atoi(arr[2]);
			all->face->point.z = ft_atoi(arr[3]);
		}
		else if (ft_strequ(arr[0], "rotation"))
		{
			all->face->rotation.x = ft_atoi(arr[1]);
			all->face->rotation.y = ft_atoi(arr[2]);
			all->face->rotation.z = ft_atoi(arr[3]);
		}
		else
			errmsg("Wrong all");
		free2arr(arr);
		i++;
	}
}

static void		parse_render(t_all *all)
{
	char	**arr;

	arr = get_line_arr(all->fd, 3);
	if (ft_strequ(arr[0], "size"))
	{
		all->scene_size.x = ft_atoi(arr[1]);
		all->scene_size.y = ft_atoi(arr[2]);
		if (all->scene_size.x < 600 || all->scene_size.x > 1920)
			errmsg("Scene height can't be lesser than 600 or bigger than 1920");
		if (all->scene_size.y < 600 || all->scene_size.y > 1368)
			errmsg("Scene width can't be lesser than 600 or bigger than 1368");
	}
	else
		errmsg("Wrong all");
	free2arr(arr);
}

static int		choose_way(t_all *all, char *str, int *save)
{
	if (ft_strequ(str, "camera:"))
	{
		*save ^= 1;
		parse_camera(all);
	}
	else if (ft_strequ(str, "render:"))
	{
		*save ^= 2;
		parse_render(all);
	}
	else if (ft_strequ(str, "name:"))
	{
		*save ^= 4;
		all->name = get_line(all->fd);
	}
	else if (ft_strequ(str, "{"))
		return (0);
	else if (ft_strequ(str, "}"))
		return (1);
	return (0);
}

void			scene_parser(t_all *all)
{
	char		*str;
	int			save;

	save = 0;
	while (1)
	{
		str = get_line(all->fd);
		if (choose_way(all, str, &save))
		{
			free(str);
			break ;
		}
		free(str);
	}
	if (save != 7)
		errmsg("Do not have enough information to create scene");
}
