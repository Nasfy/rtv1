/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   readfile_objects_parser_help.c                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiriuk <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 16:01:34 by abiriuk           #+#    #+#             */
/*   Updated: 2018/11/25 18:10:24 by abiriuk          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

int			help(t_figure *obj, char **const arr)
{
	if (ft_strequ(arr[0], "]"))
		return (0);
	if (ft_strequ(arr[0], "rgb"))
		obj->color = new_color(ft_atoi(arr[1]), ft_atoi(arr[2]),
					ft_atoi(arr[3]));
	else if (ft_strequ(arr[0], "specularity"))
	{
		obj->specularity = ft_atof(arr[1]);
		if (obj->specularity != -1 && obj->specularity < 0)
			errmsg("Specularity of objects can be 0 and bigger, or -1");
	}
	return (1);
}
