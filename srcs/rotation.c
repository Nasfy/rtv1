/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rotation.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiriuk <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 16:03:14 by abiriuk           #+#    #+#             */
/*   Updated: 2018/11/25 18:12:22 by abiriuk          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"
#include <math.h>

static cl_double4	rotate_z(cl_double4 rotate, cl_double4 ray)
{
	const double	cos_z = cos(rotate.z);
	const double	sin_z = sin(rotate.z);

	rotate.x = (ray.x * cos_z) - (ray.y * sin_z);
	rotate.y = (ray.x * sin_z) + (ray.y * cos_z);
	rotate.z = ray.z;
	return (rotate);
}

static cl_double4	rotate_xy(cl_double4 rotate, cl_double4 ray)
{
	const double	cos_x = cos(rotate.x);
	const double	sin_x = sin(rotate.x);
	const double	cos_y = cos(rotate.y);
	const double	sin_y = sin(rotate.y);

	rotate.y = (ray.y * cos_x) - (ray.z * sin_x);
	rotate.z = (ray.y * sin_x) + (ray.z * cos_x);
	rotate.x = ray.x;
	ray = rotate;
	rotate.x = (ray.x * cos_y) + (ray.z * sin_y);
	rotate.z = (-ray.x * sin_y) + (ray.z * cos_y);
	rotate.y = ray.y;
	return (rotate);
}

cl_double4			rotate_vector(cl_double4 vector, cl_double4 rotate)
{
	vector = rotate_xy(rotate, vector);
	vector = rotate_z(rotate, vector);
	return (vector);
}
