/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   readfile_light_ambient.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: abiriuk <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/11/25 15:58:49 by abiriuk           #+#    #+#             */
/*   Updated: 2018/11/25 18:08:58 by abiriuk          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "raytracer.h"

int				ambient_lines(t_all *all)
{
	char **const	arr = get_line_arr(all->fd, 4);
	int				ret;

	ret = 1;
	if (ft_strequ(arr[0], "]"))
		ret = 0;
	else if (ft_strequ(arr[0], "brightness"))
	{
		all->ambient_light = ft_atof(arr[1]);
		if (all->ambient_light < 0 || all->ambient_light > 1)
			errmsg("Light intensity can have value from 0 to 1");
	}
	else
		errmsg("Wrong information in ambient light representation");
	free2arr(arr);
	return (ret);
}

void			parse_ambient(t_all *all, t_light *light)
{
	if (light)
		;
	while (ambient_lines(all))
		;
}
