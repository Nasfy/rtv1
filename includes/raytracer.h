/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   raytracer.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: bcherkas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/02 19:49:48 by bcherkas          #+#    #+#             */
/*   Updated: 2018/11/25 18:47:12 by abiriuk          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef RAYTRACER_H
# define RAYTRACER_H

# include "libft.h"
# include "mlx.h"
# include "key_hooks.h"

# include <OpenCL/opencl.h>

# define SPHERE 1
# define PLANE 2
# define CYLINDER 3
# define CONE 4

# define USAGE ("usage: ./RTv1 \"scene.rt\"\n")

typedef struct	s_line
{
	cl_double4		start;
	double			len;
	cl_double4		ray;
	cl_double4		dest;
}				t_ray;

typedef struct	s_figure
{
	cl_double4		center;
	cl_int4			color;
	double			tg;
	int				specularity;
	cl_double4		axis;
	cl_double4		normal;
	int				type;
	double			pow_radius;
}				t_figure;

typedef struct	s_camera
{
	double			screen_distance;
	double			screen_width;
	double			screen_height;
	cl_double4		point;
	cl_double4		rotation;
	int				map_x;
	int				map_y;
	double			rel_x;
	double			rel_y;
}				t_camera;

typedef struct	s_light
{
	double			intensity;
	int				type;
	cl_double4		position;
}				t_light;

typedef struct	s_picture
{
	int				pix_size;
	void			*ptr;
	int				*arr;
	int				size;
}				t_picture;

typedef struct	s_all
{
	t_camera		*face;
	t_picture		*pic;
	t_figure		*objs;
	t_light			*lights;
	t_list			*objlist;
	t_list			*lightlist;
	cl_int4			scene_size;
	void			*mlx;
	void			*win;
	char			*name;
	double			ambient_light;
	int				num_objs;
	int				num_lights;
	int				num_pixels;
	int				fd;
}				t_all;

cl_double4		view3d(t_camera *camera, int y, int x);

void			zloop(t_all *all);
cl_int4			calc_color(t_ray *ray, double distance, t_figure *obj);

double			get_radian(double angle);
int				sq_equation(cl_double4 k, double *res);

cl_double4		rotate_vector(cl_double4 vector, cl_double4 rotate);
int				cam_move(int key, void *p);

t_all			*get_all(t_all *all);
void			errmsg(const char *str);

void			convert_to_array(t_list *obj, t_list *light, t_all *all);

int				rgb_to_int(cl_int4 c);
double			get_radian(double angle);

cl_double4		new_vector(double x, double y, double z);
cl_int4			new_color(int red, int green, int blue);
t_ray			new_line(cl_double4 start, cl_double4 direction);
t_ray			new_line2(cl_double4 start, cl_double4 direction);
cl_double4		ray_mult(cl_double4 ray, double num);
cl_double4		vector_add(cl_double4 ray, cl_double4 num);
cl_double4		vector_sub(cl_double4 ray, cl_double4 num);
double			ray_product(cl_double4 vect1, cl_double4 vect2);
double			ray_pow(cl_double4 vect);
double			ray_length(cl_double4 vect);
double			ray_cos(cl_double4 vect1, cl_double4 vect2);
cl_double4		ray_normalize(cl_double4 vect);

void			parsing(t_all *all, char *str);
void			scene_parser(t_all *all);
void			light_parser(t_all *all);
void			objects_parser(t_all *all);

char			*trim_str(char *str);
char			*get_line(int fd);
char			**get_line_arr(int fd, int len);
int				len2arr(char **arr);
void			free2arr(char **arr);

void			read_sphere(t_all *all, t_figure *figure);
void			read_plane(t_all *all, t_figure *figure);
void			read_cylinder(t_all *all, t_figure *figure);
void			read_cone(t_all *all, t_figure *figure);
int				help(t_figure *figure, char **arr);

int				choose_intersect(t_figure *obj, t_ray *ray, double *t);
cl_double4		choose_normal(t_figure *save, t_ray *ray, double distance);

int				cone_intersection(t_figure *figure, t_ray *ray, double *t);
cl_double4		cone_normal(t_figure *obj, t_ray *ray, double d);
int				cylinder_intersection(t_figure *figure, t_ray *ray, double *t);
cl_double4		cyl_normal(t_figure *obj, t_ray *ray, double d);
int				plane_intersection(t_figure *figure, t_ray *ray, double *t);
cl_double4		plane_normal(t_figure *obj, t_ray *ray);
int				sphere_intersection(t_figure *figure, t_ray *ray, double *t);
cl_double4		sphere_normal(t_figure *obj, t_ray *ray);

int				light_help(t_light *light, char **arr);
void			parse_direct(t_all *all, t_light *light);
void			parse_dot(t_all *all, t_light *light);
void			parse_ambient(t_all *all, t_light *light);
void			parse_projector(t_all *all, t_light *light);

#endif
